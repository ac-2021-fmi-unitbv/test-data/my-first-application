package com.example.myapplication.interfaces

interface ActivityFragmentCommunication {
    fun onReplaceFragment(TAG: String)
}