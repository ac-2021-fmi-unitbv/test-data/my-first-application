package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.myapplication.interfaces.ActivityFragmentCommunication
import kotlinx.android.synthetic.main.activity_lab2.*

class Lab2Activity : AppCompatActivity(), ActivityFragmentCommunication {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lab2)

        Log.e(Lab2Activity::class.java.name, "onCreate")

        replaceWithFirstFragment()

        btn_test.setOnClickListener {
            addSecondFragment()
        }
    }

    override fun onStart() {
        super.onStart()

        Log.e(Lab2Activity::class.java.name, "onStart")
    }

    override fun onResume() {
        super.onResume()

        Log.e(Lab2Activity::class.java.name, "onResume")
    }

    override fun onPause() {
        super.onPause()

        Log.e(Lab2Activity::class.java.name, "onPause")
    }

    override fun onStop() {
        super.onStop()

        Log.e(Lab2Activity::class.java.name, "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()

        Log.e(Lab2Activity::class.java.name, "onDestroy")
    }

    fun goToMainActivity() {
        val intent: Intent = Intent(this, MainActivity::class.java)

        startActivity(intent)

        finish()
    }

    fun replaceWithFirstFragment() {
        val fragmentManager = supportFragmentManager

        val transaction = fragmentManager.beginTransaction()

        val firstFragment = FirstFragment.newInstance()

        val TAG = FirstFragment::class.java.name

        val replaceTransaction = transaction.add(
            R.id.fl_container,
            firstFragment,
            TAG
        )

        replaceTransaction.commit()
    }

    fun addSecondFragment() {
        val fragmentManager = supportFragmentManager

        val transaction = fragmentManager.beginTransaction()

        val thirdFragment = SecondFragment.newInstance()

        val TAG = SecondFragment::class.java.name

        val addTransaction = transaction.add(
            R.id.fl_container,
            thirdFragment,
            TAG
        )

        addTransaction.addToBackStack(TAG)

        addTransaction.commit()
    }

    fun replaceWithThirdFragment() {
        val fragmentManager = supportFragmentManager

        val transaction = fragmentManager.beginTransaction()

        val thirdFragment = ThirdFragment.newInstance()

        val TAG = ThirdFragment::class.java.name

        val replaceTransaction = transaction.replace(
            R.id.fl_container,
            thirdFragment,
            TAG
        )

        replaceTransaction.addToBackStack(TAG)

        replaceTransaction.commit()
    }

    override fun onReplaceFragment(TAG: String) {
        val fragmentManager = supportFragmentManager

        val transaction = fragmentManager.beginTransaction()

        val fragment = when (TAG) {
            FirstFragment::class.java.name ->
                FirstFragment.newInstance()

            SecondFragment::class.java.name ->
                SecondFragment.newInstance()

            ThirdFragment::class.java.name ->
                ThirdFragment.newInstance()

            else -> ThirdFragment.newInstance()
        }

        val replaceTransaction = transaction.replace(
            R.id.fl_container,
            fragment,
            TAG
        )

        replaceTransaction.addToBackStack(TAG)

        replaceTransaction.commit()
    }
}