package com.example.myapplication

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import com.example.myapplication.interfaces.ActivityFragmentCommunication
import kotlinx.android.synthetic.main.activity_lab2.*
import kotlinx.android.synthetic.main.fragment_first.*

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    var activityFragmentCommunication: ActivityFragmentCommunication? = null

    companion object {
        fun newInstance() : Fragment {
            return FirstFragment()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        when(context) {
            is ActivityFragmentCommunication ->
                activityFragmentCommunication = context
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button_first.setOnClickListener {
            activityFragmentCommunication?.onReplaceFragment(ThirdFragment::class.java.name)
        }
    }
}